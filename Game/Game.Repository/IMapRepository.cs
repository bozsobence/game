﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Model;

namespace Game.Repository
{
    public interface IMapRepository
    {
        List<string> GetSavedMaps();
        void SaveXML(string mapName);
        Map ActualMap { get; set; }
        void LoadMap(string mapName);
        void AddElement(GameElement e);
    }
}
