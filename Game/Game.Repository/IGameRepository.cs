﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Model;

namespace Game.Repository
{
    public interface IGameRepository
    {
        List<string> GetSavedGames();
        Map ImportXML(string mapName);
        void ExportXML(Map map);
        void SetGameModel(Map map);
        List<GameElement> GetElements();
    }
}
