﻿using Game.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Reflection;

namespace Game.Repository
{
    public class GameRepository : IGameRepository
    {
        private IGameModel model;
        public GameRepository(IGameModel model)
        {
            this.model = model;
        }
        public void ExportXML(Map map)
        {
            
            XDocument xdoc = new XDocument();
            xdoc.Add(new XElement("elements", new XAttribute("mapName", map.Name), new XAttribute("count", map.GameElements.Count)));
            foreach (var item in map.GameElements)
            {
                XElement gameElement = new XElement("element",
                    new XElement("posX", item.X),
                    new XElement("posY", item.Y),
                    new XElement("width", item.Area.Width),
                    new XElement("height", item.Area.Height),
                    new XElement("type", item.GetType().Name));
                xdoc.Root.Add(gameElement);
            }
            xdoc.Save($"{map.Name}.save");
        }

        public List<GameElement> GetElements()
        {

            return model.GameElements;
        }

        public List<string> GetSavedGames()
        {
            List<FileInfo> files = new DirectoryInfo(Directory.GetCurrentDirectory()).GetFiles("*.save").ToList();
            return files.Select(x => x.FullName).ToList();
        }

        public Map ImportXML(string mapName)
        {
            XDocument xdoc = XDocument.Load(mapName);
            Map map = new Map();
            map.Name = xdoc.Root.Attribute("mapName").Value;
            foreach (var item in xdoc.Root.Descendants("element"))
            {
                double x = double.Parse(item.Element("posX").Value);
                double y = double.Parse(item.Element("posY").Value);
                double w = double.Parse(item.Element("width").Value);
                double h = double.Parse(item.Element("height").Value);
                string type = item.Element("type").Value;

                switch (type)
                {
                    case "Player":
                        Player player = new Player(x, y, w, h, 0, 0);
                        map.Player = player;
                        break;
                    case "Wall":
                        Wall wall = new Wall(x, y, w, h);
                        map.GameElements.Add(wall);
                        break;
                    case "Exit":
                        Exit exit = new Exit(x, y, w, h);
                        map.GameElements.Add(exit);
                        break;

                    default:
                        GameElement gameElement = new GameElement(x, y, w, h);
                        map.GameElements.Add(gameElement);
                        break;
                }

            }
            return map;
        }

        public void SetGameModel(Map map)
        {
            model.Player = map.Player;
            model.GameElements = map.GameElements;
            
        }
    }
}
