﻿using Game.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Game.Repository
{
    public class MapRepository : IMapRepository
    {
        public Map ActualMap { get; set; }

        public void AddElement(GameElement e)
        {
            ActualMap.GameElements.Add(e);
        }

        public List<string> GetSavedMaps()
        {
            List<FileInfo> files = new DirectoryInfo(Directory.GetCurrentDirectory()).GetFiles("*.map").ToList();
            return files.Select(x => x.FullName).ToList();
        }

        public void LoadMap(string mapName)
        {
            XDocument xdoc = XDocument.Load(mapName);
            ActualMap.Name = xdoc.Root.Attribute("mapName").Value;
            foreach (var item in xdoc.Root.Descendants("element"))
            {
                double x = double.Parse(item.Element("posX").Value);
                double y = double.Parse(item.Element("posY").Value);
                double w = double.Parse(item.Element("width").Value);
                double h = double.Parse(item.Element("height").Value);
                string type = item.Element("type").Value;

                switch (type)
                {
                    case "Player":
                        Player player = new Player(x, y, w, h, 0, 0);
                        this.ActualMap.Player = player;
                        break;
                    case "Wall":
                        Wall wall = new Wall(x, y, w, h);
                        this.ActualMap.GameElements.Add(wall);
                        break;
                    case "Exit":
                        Exit exit = new Exit(x, y, w, h);
                        this.ActualMap.GameElements.Add(exit);
                        break;

                    default:
                        GameElement gameElement = new GameElement(x, y, w, h);
                        this.ActualMap.GameElements.Add(gameElement);
                        break;
                }

            }
        }

        public void SaveXML(string mapName)
        {
            ActualMap.Name = mapName;
            XDocument xdoc = new XDocument();
            xdoc.Add(new XElement("elements", new XAttribute("mapName", ActualMap.Name), new XAttribute("count", ActualMap.GameElements.Count)));
            foreach (var item in ActualMap.GameElements)
            {
                XElement gameElement = new XElement("element",
                    new XElement("posX", item.X),
                    new XElement("posY", item.Y),
                    new XElement("width", item.Area.Width),
                    new XElement("height", item.Area.Height),
                    new XElement("type", item.GetType().Name));
                xdoc.Root.Add(gameElement);
            }
            xdoc.Save($"{ActualMap.Name}.map");
        }
    }
}
