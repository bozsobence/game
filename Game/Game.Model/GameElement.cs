﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Game.Model
{
    public class GameElement
    {
		Rect area;
		private double x;
		private double y;
		private double width;
		private double height;
		private BitmapImage image;

		public BitmapImage Image
		{
			get { return image; }
			set { image = value; }
		}


		public double Height
		{
			get { return height; }
			set { height = value; }
		}

		public double Width
		{
			get { return width; }
			set { width = value; }
		}

		public double Y
		{
			get { return y; }
			set { y = value; }
		}

		public Rect Area
		{
			get { return area; }
		}

		public double X
		{
			get { return x; }
			set { x = value; }
		}
		public GameElement(double x, double y, double w, double h)
		{
			this.x = x;
			this.y = y;
			this.width = w;
			this.height = h;
			area = new Rect(this.x, this.y, this.width, this.height);
		}
	}
}
