﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
    public class Map
    {
        public List<GameElement> GameElements { get; set; }
        public Player Player { get; set; }
        public string Name { get; set; }
        public Map()
        {
            GameElements = new List<GameElement>();
        }

    }
}
