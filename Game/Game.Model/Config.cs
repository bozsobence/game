﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Game.Model
{

    public static class Config
    {
        public const int ElementSize = 40;
        public static double GameWidth = 1920;
        public static double GameHeight = 1080;
        public const int DefaultXMove = 4;
        public const int DefaultYMove = 12;
        public static Brush FloorBrush = Brushes.Black;
        public static Brush PlayerBrush = Brushes.Blue;
        public static Brush FinishBrush = Brushes.Green;
    }
}
