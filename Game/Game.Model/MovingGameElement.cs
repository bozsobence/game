﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
	public class MovingGameElement : GameElement
	{
		private double dx;
		private double dy;

		public double Dy
		{
			get { return dy; }
			set { dy = value; }
		}

		public double Dx
		{
			get { return dx; }
			set { dx = value; }
		}
		public MovingGameElement(double x, double y, double w, double h, double dx, double dy) : base(x, y, w, h)
		{
			this.dx = dx;
			this.dy = dy;
		}
	}
}
