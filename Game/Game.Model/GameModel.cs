﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
    public class GameModel : IGameModel
    {
        public List<GameElement> GameElements { get; set; }
        public Player Player { get; set; }
        public bool GameEnded { get; set; }
        public GameState GameState { get; set; }
        public GameModel()
        {
            GameElements = new List<GameElement>();
        }
    }
}
