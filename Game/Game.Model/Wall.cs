﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
    public class Wall : StaticGameElement
    {
        public Wall(double x, double y, double w, double h) : base(x, y, w, h)
        {

        }
    }
}
