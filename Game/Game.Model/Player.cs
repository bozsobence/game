﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
    public class Player : MovingGameElement
    {
        public int CountOfLives { get; set; }

        private List<GameElement> collectedItems;

        public int CurrentScore { get; set; }

        public Player(double x, double y, double w, double h, double dx, double dy) : base(x, y, w, h, dx, dy)
        {
            collectedItems = new List<GameElement>();
        }
    }
}
