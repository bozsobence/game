﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
    public interface IGameModel
    {
        List<GameElement> GameElements { get; set; }
        Player Player { get; set; }
        bool GameEnded { get; set; }
        GameState GameState { get; set; }
    }
}
