﻿using Game.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Repository;

namespace Game.Logic
{
    public class MapEditorLogic : IMapEditorLogic
    {
        public event EventHandler MapSaved;
        public event EventHandler MapLoaded;
        private IMapRepository mapRepo;
        public MapEditorLogic()
        {
            this.mapRepo = new MapRepository();
        }

        public void AddExit(double x, double y)
        {
            this.mapRepo.AddElement(new Exit(x, y, Config.ElementSize, Config.ElementSize));
        }

        public void AddPlayer(double x, double y)
        {
            this.mapRepo.AddElement(new Player(x, y, Config.ElementSize, Config.ElementSize, 0, 0));
        }

        public void AddWall(double x, double y, double w, double h)
        {
            this.mapRepo.AddElement(new Wall(x, y, w, h));
        }

        public void LoadMap(string mapName)
        {
            this.mapRepo.LoadMap(mapName);
        }

        public void SaveMap(string mapName)
        {
            this.mapRepo.SaveXML(mapName);
        }

        public List<string> GetSavedMaps()
        {
            return this.mapRepo.GetSavedMaps();
        }
    }
}
