﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Game.Model;

namespace Game.Logic
{
    public interface IControlLogic
    {
        List<string> LoadMap();
        void LoadSelected(string mapName);
        void LeftMouseButtonPressed(double x, double y, GameElementType type);
        void MovementKeyPressed(Key k);
        bool SaveGame();
        List<string> LoadGame();
        void LoadSelectedGame(string game);
        bool SaveMap(string mapName);
        List<GameElement> GetElements();

    }
}
