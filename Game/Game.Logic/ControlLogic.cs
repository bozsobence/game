﻿using Game.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Game.Logic
{
    public class ControlLogic : IControlLogic
    {
        private IGameLogic gameLogic;
        private IMapEditorLogic mapLogic;
        public ControlLogic(IGameLogic gameLogic, IMapEditorLogic mapLogic)
        {
            this.gameLogic = gameLogic;
            this.mapLogic = mapLogic;
        }
        public List<GameElement> GetElements()
        {
            return this.gameLogic.GetElements();
        }

        public void LeftMouseButtonPressed(double x, double y, GameElementType type)
        {
            switch (type)
            {
                case GameElementType.Wall:
                    this.mapLogic.AddWall(x, y, Config.ElementSize, Config.ElementSize);
                    break;
                case GameElementType.Player:
                    this.mapLogic.AddPlayer(x, y);
                    break;
                case GameElementType.Exit:
                    this.mapLogic.AddExit(x, y);
                    break;
                default:
                    break;
            }
        }

        public List<string> LoadGame()
        {
            return this.gameLogic.GetSavedGames();
        }

        public List<string> LoadMap()
        {
            return this.mapLogic.GetSavedMaps();
        }

        public void LoadSelected(string mapName)
        {
            this.mapLogic.LoadMap(mapName);
        }

        public void LoadSelectedGame(string game)
        {
            this.gameLogic.LoadSelectedGame(game);
        }

        public void MovementKeyPressed(Key k)
        {
            switch (k)
            {
                case Key.W:
                    this.gameLogic.Jump();
                    break;
                case Key.A:
                    this.gameLogic.MoveLeft();
                    break;
                case Key.D:
                    this.gameLogic.MoveRight();
                    break;
                default:
                    break;
            }
        }

        public bool SaveGame()
        {
            try
            {
                this.gameLogic.SaveGame();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        public bool SaveMap(string mapName)
        {
            try
            {
                this.mapLogic.SaveMap(mapName);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }
    }
}
