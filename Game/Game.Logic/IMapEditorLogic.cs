﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Model;

namespace Game.Logic
{
    public interface IMapEditorLogic
    {
        void AddWall(double x, double y, double w, double h);
        void AddPlayer(double x, double y);
        void AddExit(double x, double y);
        void SaveMap(string mapName);
        void LoadMap(string mapName);
        List<string> GetSavedMaps();
        event EventHandler MapSaved;
        event EventHandler MapLoaded;
    }
}
