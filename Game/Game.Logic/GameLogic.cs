﻿using Game.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Repository;

namespace Game.Logic
{
    public class GameLogic : IGameLogic
    {
        public event EventHandler RefreshScreen;
        public event EventHandler GameSaved;
        public event EventHandler GameLoaded;
        private IGameRepository gameRepo;
        private IGameModel model;
        public GameLogic(IGameModel model)
        {
            this.model = model;
            gameRepo = new GameRepository(this.model);
        }
        public List<GameElement> GetElements()
        {
            return gameRepo.GetElements();
        }

        public List<string> GetSavedGames()
        {
            return gameRepo.GetSavedGames();
        }

        public void Jump()
        {
            throw new NotImplementedException();
        }

        public void LoadSelectedGame(string name)
        {
            gameRepo.SetGameModel(gameRepo.ImportXML(name));
        }

        public void MoveLeft()
        {
            throw new NotImplementedException();
        }

        public void MoveRight()
        {
            throw new NotImplementedException();
        }

        public void SaveGame()
        {
            string name = DateTime.Now.ToString();
            Map m = new Map()
            {
                Name = name,
                Player = model.Player,
                GameElements = model.GameElements
            };
            gameRepo.ExportXML(m);
        }
    }
}
