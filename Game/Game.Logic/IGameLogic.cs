﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Model;

namespace Game.Logic
{
    public interface IGameLogic
    {
        List<GameElement> GetElements();
        void MoveRight();
        void MoveLeft();
        void Jump();
        event EventHandler RefreshScreen;
        List<string> GetSavedGames();
        void LoadSelectedGame(string name);
        void SaveGame();
        event EventHandler GameSaved;
        event EventHandler GameLoaded;

    }
}
