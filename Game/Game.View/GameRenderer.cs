﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Game.Model;

namespace Game.View
{
    public class GameRenderer
    {
        private IGameModel model;
        public GameRenderer(IGameModel model)
        {
            this.model = model;
        }

        public void Draw(DrawingContext ctx)
        {
            DrawingGroup dg = new DrawingGroup();
            ctx.DrawRectangle(new ImageBrush(new BitmapImage(new Uri("background.bmp", UriKind.RelativeOrAbsolute))), null, new Rect(0, 0, Config.GameWidth, Config.GameHeight));
            foreach (var item in model.GameElements)
            {
                if (item is Wall)
                {
                    GeometryDrawing floor = new GeometryDrawing(Config.FloorBrush, new Pen(Config.FloorBrush, 1), new RectangleGeometry(item.Area));
                    dg.Children.Add(floor);
                }
                else if (item is Exit)
                {
                    GeometryDrawing exit = new GeometryDrawing(Config.FinishBrush, new Pen(Config.FinishBrush, 1), new RectangleGeometry(item.Area));
                    dg.Children.Add(exit);
                }
                
            }
            GeometryDrawing player = new GeometryDrawing(Config.PlayerBrush, new Pen(Config.PlayerBrush, 1), new EllipseGeometry(model.Player.Area));
            dg.Children.Add(player);
            ctx.DrawDrawing(dg);
        }
    }
}
