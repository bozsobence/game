﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Model;
using Game.Logic;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Media;

namespace Game.View
{
    public class GameControl : FrameworkElement
    {
        private IGameModel model;
        private IMapEditorLogic mapLogic;
        private IGameLogic gameLogic;
        private GameRenderer renderer;
        private DispatcherTimer timer;
        public GameControl()
        {
            Loaded += GameControl_Loaded;
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            model = new GameModel();
            mapLogic = new MapEditorLogic();
            gameLogic = new GameLogic(model);
            gameLogic.LoadSelectedGame("map.save");
            renderer = new GameRenderer(model);
            Window win = Window.GetWindow(this);
            gameLogic.RefreshScreen += (obj, args) => InvalidateVisual();
            if (win != null)
            {
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromMilliseconds(30);
                timer.Tick += Timer_Tick;
                timer.Start();

            }
            InvalidateVisual();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            InvalidateVisual();
        }
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (renderer != null)
            {
                renderer.Draw(drawingContext);
            }
        }
    }
}
